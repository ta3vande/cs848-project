CS 848 Final Project
====================

Repositories
------------
- This repo: A modified version of [Clonos](https://github.com/delftdata/Clonos) forked from the main repo. This contains our changes described in our project report.
- [cs848-testing-scripts](https://gitlab.uwaterloo.ca/a82yu/cs848-testing-scripts) is forked from [this repo](https://github.com/delftdata/flink-test-scripts) with some modifications to run the experiments we used.
- [cs848-project-beam](https://gitlab.uwaterloo.ca/a82yu/cs848-project-beam) is forked from [this repo](https://github.com/delftdata/beam) with some modifications in clonos-update branch to build a Clonos runner for benchmarking with NEXMark implementation. No longer used after [this repo](https://github.com/PSilvestre/ClonosReproducibility) was created.

Branches
--------
- `cs848-base`: contains some scripts for testing and extra logging used to time different parts of the recovery for evaluation.
- `faster-recovery`: contains the changes implemented to speed up Clonos' recovery.
- `flink1.7` and `clonos` are unmodified branches from the original Clonos repository, and contain the base versions of flink 1.7 and clonos respectively.
- `clonos-active-replication` is the work we did initially to try getting an active standby to work. Some stuff from here was copied to other branches.
- `hybrid-checkpoint` is the work we did to try implementing a ``hybrid'' checkpoint where the primary only stores checkpoints in memory and the standby persists it to the file system after restoring it.
