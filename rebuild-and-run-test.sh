#!/bin/bash

################################################################################
#  Licensed to the Apache Software Foundation (ASF) under one
#  or more contributor license agreements.  See the NOTICE file
#  distributed with this work for additional information
#  regarding copyright ownership.  The ASF licenses this file
#  to you under the Apache License, Version 2.0 (the
#  "License"); you may not use this file except in compliance
#  with the License.  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
# limitations under the License.
################################################################################

clonos_dir=~/Clonos
test_scripts_dir=~/flink-test-scripts


build_subproj() {
  pushd $1 && mvn install -DskipTests -Dcheckstyle.skip && popd
  if [[ $? -ne 0 ]]; then
    echo "COMPILE ERROR -- maybe need to mvn clean first"
    exit
  fi
}


# build clonos
pushd $clonos_dir
build_subproj .

# TODO building the individual projects doesn't seem to work, find out if there is a way to speed this up.
# build_subproj flink-runtime
# build_subproj flink-streaming-java
# build_subproj flink-dist
popd

# Build the docker image
pushd $clonos_dir/flink-contrib/docker-flink
./build.sh --from-local-dist --image-name cs848clonos
if [[ $? -ne 0 ]]; then
  echo "ERROR BUILDING DOCKER IMAGE"
  exit
fi
popd

# Run the experiment
pushd $test_scripts_dir
./run-recovery-experiment.sh

# Stop the cluster --- skipping this so we can check the flink UI
#docker-compose down
